package wwc2019v2.command;

import wwc2019v2.model.Group;

import java.io.IOException;
import java.util.List;

public interface Command<T> {

    T fetch() throws IOException;
}

