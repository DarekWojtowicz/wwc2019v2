package wwc2019v2.command;

import com.google.gson.Gson;
import wwc2019v2.model.Group;
import wwc2019v2.network.HttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class GetGroupResultsCommand implements Command<List<Group>> {

    private HttpClient httpClient;

    public GetGroupResultsCommand(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public List<Group> fetch() throws IOException {
        Gson gson = new Gson();
        String response = httpClient.makeRequest(
                "https://worldcup.sfg.io/teams/group_results");
        Group[] groups = gson.fromJson(response, Group[].class);
        return Arrays.asList(groups);
    }
}
