package wwc2019v2.usecase;

import wwc2019v2.model.Group;

import java.util.List;

public class GroupResultUseCase {

    public static void teamWithNoDrows(List<Group> lista){
        System.out.println("\n Znajdź wszystkie zespoły, które przynajmniej raz zremisowały\n");
        lista.stream()
                .flatMap(gl -> gl.getTeams().stream())
                .filter(tl -> tl.getDraws() == 0)
                .forEach(System.out::println);
    }

    public static void teamsWithAtLeast3Wins(List<Group> lista){
        System.out.println("\n Znajdź wszystkie zespoły, które wygrały co najmniej 3 mecze\n");
        lista.stream()
                .flatMap(gl -> gl.getTeams().stream())
                .filter(tl -> tl.getWins() >= 3)
                .forEach(System.out::println);
    }

    public static void teamsWithAtLeast2Looses(List<Group> lista){
        System.out.println("\n Znajdź wszystkie zespoły z conajmniej 2 przegranymi na koncie\n");
        lista.stream()
                .flatMap(gl -> gl.getTeams().stream())
                .filter(tl -> tl.getLooses() >= 2)
                .forEach(System.out::println);
    }

    public static void teamsWithNegativeScoreBalance(List<Group> lista){
        System.out.println("\n Znajdź wszystkie zespoły z większą liczbą straconych bramek niż strzelonych\n");
        lista.stream()
                .flatMap(gl -> gl.getTeams().stream())
                .filter(tl -> tl.getGoalDifferential() < 0)
                .forEach(System.out::println);
    }
}
