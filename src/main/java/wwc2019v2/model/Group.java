package wwc2019v2.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Group {

    private int id;
    private String letter;
    @SerializedName("ordered_teams")
    private List<Team> teams;

    public Group(int id, String letter, List<Team> teams) {
        this.id = id;
        this.letter = letter;
        this.teams = teams;
    }

    public Group() {
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", letter='" + letter + '\'' +
                ", teams=" + teams +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }
}
