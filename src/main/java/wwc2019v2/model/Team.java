package wwc2019v2.model;

import com.google.gson.annotations.SerializedName;

public class Team {
    private int id;
    private String country;
    @SerializedName("alternate_name")
    private String altarnateName;
    @SerializedName("group_id")
    private int groupID;
    @SerializedName("group_letter")
    private String groupLetter;
    private int wins;
    private int draws;
    private int losses;
    @SerializedName("games_played")
    private int gamesPlayed;
    private int points;
    @SerializedName("goals_for")
    private int goalsFor;
    @SerializedName("goals_against")
    private int goalsAgainst;
    @SerializedName("goal_differential")
    private int goalDifferential;

    public Team(int id, String country, String altarnateName, int groupID, String groupLetter, int wins, int draws, int looses, int gamesPlayed, int points, int goalsFor, int goalsAgainst, int goalDifferential) {
        this.id = id;
        this.country = country;
        this.altarnateName = altarnateName;
        this.groupID = groupID;
        this.groupLetter = groupLetter;
        this.wins = wins;
        this.draws = draws;
        this.losses = losses;
        this.gamesPlayed = gamesPlayed;
        this.points = points;
        this.goalsFor = goalsFor;
        this.goalsAgainst = goalsAgainst;
        this.goalDifferential = goalDifferential;
    }

    public Team() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAltarnateName() {
        return altarnateName;
    }

    public void setAltarnateName(String altarnateName) {
        this.altarnateName = altarnateName;
    }

    public int getGroupID() {
        return groupID;
    }

    public void setGroupID(int groupID) {
        this.groupID = groupID;
    }

    public String getGroupLetter() {
        return groupLetter;
    }

    public void setGroupLetter(String groupLetter) {
        this.groupLetter = groupLetter;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public int getLooses() {
        return losses;
    }

    public void setLooses(int looses) {
        this.losses = looses;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGoalsFor() {
        return goalsFor;
    }

    public void setGoalsFor(int goalsFor) {
        this.goalsFor = goalsFor;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getGoalDifferential() {
        return goalDifferential;
    }

    public void setGoalDifferential(int goalDifferential) {
        this.goalDifferential = goalDifferential;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", altarnateName='" + altarnateName + '\'' +
                ", groupID=" + groupID +
                ", groupLetter='" + groupLetter + '\'' +
                ", wins=" + wins +
                ", draws=" + draws +
                ", losses=" + losses +
                ", gamesPlayed=" + gamesPlayed +
                ", points=" + points +
                ", goalsFor=" + goalsFor +
                ", goalsAgainst=" + goalsAgainst +
                ", goalDifferential=" + goalDifferential +
                '}';
    }
}
