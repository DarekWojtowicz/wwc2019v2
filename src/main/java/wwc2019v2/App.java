package wwc2019v2;

import wwc2019v2.command.Command;
import wwc2019v2.command.GetGroupResultsCommand;
import wwc2019v2.model.Group;
import wwc2019v2.model.Group;
import wwc2019v2.model.Team;
import wwc2019v2.network.HttpClient;
import wwc2019v2.usecase.GroupResultUseCase;


import java.io.IOException;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args ) throws IOException {
        HttpClient httpClient = new HttpClient();
        Command<List<Group>> getGroupResultsCommand =
                new GetGroupResultsCommand(httpClient);
        List<Group> groups = getGroupResultsCommand.fetch();

        GroupResultUseCase.teamWithNoDrows(groups);
        GroupResultUseCase.teamsWithAtLeast3Wins(groups);
        GroupResultUseCase.teamsWithAtLeast2Looses(groups);
        GroupResultUseCase.teamsWithNegativeScoreBalance(groups);






    }
}