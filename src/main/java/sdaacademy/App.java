package sdaacademy;

import sdaacademy.command.Command;
import sdaacademy.command.GetGroupResultsCommand;
import sdaacademy.command.GetMatchesCommand;
import sdaacademy.command.GetTeamsCommand;
import sdaacademy.model.Group;
import sdaacademy.model.Match;
import sdaacademy.model.Team;
import sdaacademy.network.HttpClient;
import sdaacademy.usecase.GroupResultUseCase;
import sdaacademy.usecase.MatchUseCase;
import sdaacademy.usecase.TeamUseCase;

import java.io.IOException;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        HttpClient httpClient = new HttpClient();
        Command<Group> getGroupResultsCommand =
                new GetGroupResultsCommand(httpClient);

        Command<Team> getTeamsCommand = new GetTeamsCommand(httpClient);
        Command<Match> getMatchCommand = new GetMatchesCommand(httpClient);
        List<Group> groups = getGroupResultsCommand.fetch();
        List<Team> teams = getTeamsCommand.fetch();
        List<Match> matches = getMatchCommand.fetch();
        GroupResultUseCase.findTeamsWithoutDraw(groups);
        TeamUseCase.showTeams(teams);
        MatchUseCase.findTeamsWithRedCard(matches);
    }
}
