package sdaacademy.model;

import com.google.gson.annotations.SerializedName;

public class FootballPlayer {

    private String name;

    @SerializedName("captain")
    private boolean isCaptain;

    @SerializedName("shirt_number")
    private int shirtNumber;

    private String position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCaptain() {
        return isCaptain;
    }

    public void setCaptain(boolean captain) {
        isCaptain = captain;
    }

    public int getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(int shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "FootballPlayer{" +
                "name='" + name + '\'' +
                ", isCaptain=" + isCaptain +
                ", shirtNumber=" + shirtNumber +
                ", position='" + position + '\'' +
                '}';
    }
}

/*
"name": "Solene DURAND",
"captain": false,
"shirt_number": 1,
"position": "Goalie
 */