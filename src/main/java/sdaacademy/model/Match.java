package sdaacademy.model;

import com.google.gson.annotations.SerializedName;

public class Match {

    @SerializedName("home_team_statistics")
    private TeamStatistics homeTeamStatistics;

    @SerializedName("away_team_statistics")
    private TeamStatistics awayTeamStatistics;

    public TeamStatistics getHomeTeamStatistics() {
        return homeTeamStatistics;
    }

    public void setHomeTeamStatistics(TeamStatistics homeTeamStatistics) {
        this.homeTeamStatistics = homeTeamStatistics;
    }

    public TeamStatistics getAwayTeamStatistics() {
        return awayTeamStatistics;
    }

    public void setAwayTeamStatistics(TeamStatistics awayTeamStatistics) {
        this.awayTeamStatistics = awayTeamStatistics;
    }

    @Override
    public String toString() {
        return "Match{" +
                "homeTeamStatistics=" + homeTeamStatistics +
                ", awayTeamStatistics=" + awayTeamStatistics +
                '}';
    }
}
