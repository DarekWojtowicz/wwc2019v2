package sdaacademy.model;

import com.google.gson.annotations.SerializedName;

/*
"id": 1,
"country": "France",
"alternate_name": null,
"fifa_code": "FRA",
"group_id": 1,
"group_letter": "A",
"wins": 4,
"draws": 0,
"losses": 1,
"games_played": 5,
"points": 12,
"goals_for": 10,
"goals_against": 4,
"goal_differential": 6
 */
public class Team {
    private int id;
    private String country;

    @SerializedName("alternate_name")
    private String alternateName;

    @SerializedName("fifa_code")
    private String fifaCode;

    @SerializedName("group_id")
    private int groupId;

    @SerializedName("group_letter")
    private String group_letter;

    @SerializedName("games_played")
    private int gamesPlayed;

    private int wins;
    private int draws;
    private int losses;
    private int points;

    @SerializedName("goals_for")
    private int goalsFor;

    @SerializedName("goals_against")
    private int goalsAgainst;

    @SerializedName("goal_differential")
    private int goalDifferential;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAlternateName() {
        return alternateName;
    }

    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public String getFifaCode() {
        return fifaCode;
    }

    public void setFifaCode(String fifaCode) {
        this.fifaCode = fifaCode;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroup_letter() {
        return group_letter;
    }

    public void setGroup_letter(String group_letter) {
        this.group_letter = group_letter;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGoalsFor() {
        return goalsFor;
    }

    public void setGoalsFor(int goalsFor) {
        this.goalsFor = goalsFor;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getGoalDifferential() {
        return goalDifferential;
    }

    public void setGoalDifferential(int goalDifferential) {
        this.goalDifferential = goalDifferential;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", country='" + country + '\'' +
                ", alternateName='" + alternateName + '\'' +
                ", fifaCode='" + fifaCode + '\'' +
                ", groupId=" + groupId +
                ", group_letter='" + group_letter + '\'' +
                ", gamesPlayed=" + gamesPlayed +
                ", wins=" + wins +
                ", draws=" + draws +
                ", losses=" + losses +
                ", points=" + points +
                ", goalsFor=" + goalsFor +
                ", goalsAgainst=" + goalsAgainst +
                ", goalDifferential=" + goalDifferential +
                '}';
    }
}

