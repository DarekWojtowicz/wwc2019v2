package sdaacademy.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeamStatistics {

    private String country;

    @SerializedName("attempts_on_goal")
    private int attemptsOnGoal;

    @SerializedName("on_target")
    private int onTarget;

    @SerializedName("off_target")
    private int offTarget;

    private int blocked;
    private int corners;
    private int offsides;

    @SerializedName("ball_possession")
    private int ballPassion;

    @SerializedName("pass_accuracy")
    private int passAccuracy;

    @SerializedName("num_passes")
    private int numPasses;

    @SerializedName("passes_completed")
    private int passesCompleted;

    @SerializedName("distance_covered")
    private int distanceCovered;

    private int taclkes;

    private int clearances;

    @SerializedName("yellow_cards")
    private int yellowCards;

    @SerializedName("red_cards")
    private int redCards;

    @SerializedName("fouls_committed")
    private int foulsCommited;

    private String tactics;

    @SerializedName("starting_eleven")
    private List<sdaacademy.model.FootballPlayer> startingEleven;

    private List<sdaacademy.model.FootballPlayer> substitutes;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getAttemptsOnGoal() {
        return attemptsOnGoal;
    }

    public void setAttemptsOnGoal(int attemptsOnGoal) {
        this.attemptsOnGoal = attemptsOnGoal;
    }

    public int getOnTarget() {
        return onTarget;
    }

    public void setOnTarget(int onTarget) {
        this.onTarget = onTarget;
    }

    public int getOffTarget() {
        return offTarget;
    }

    public void setOffTarget(int offTarget) {
        this.offTarget = offTarget;
    }

    public int getBlocked() {
        return blocked;
    }

    public void setBlocked(int blocked) {
        this.blocked = blocked;
    }

    public int getCorners() {
        return corners;
    }

    public void setCorners(int corners) {
        this.corners = corners;
    }

    public int getOffsides() {
        return offsides;
    }

    public void setOffsides(int offsides) {
        this.offsides = offsides;
    }

    public int getBallPassion() {
        return ballPassion;
    }

    public void setBallPassion(int ballPassion) {
        this.ballPassion = ballPassion;
    }

    public int getPassAccuracy() {
        return passAccuracy;
    }

    public void setPassAccuracy(int passAccuracy) {
        this.passAccuracy = passAccuracy;
    }

    public int getNumPasses() {
        return numPasses;
    }

    public void setNumPasses(int numPasses) {
        this.numPasses = numPasses;
    }

    public int getPassesCompleted() {
        return passesCompleted;
    }

    public void setPassesCompleted(int passesCompleted) {
        this.passesCompleted = passesCompleted;
    }

    public int getDistanceCovered() {
        return distanceCovered;
    }

    public void setDistanceCovered(int distanceCovered) {
        this.distanceCovered = distanceCovered;
    }

    public int getTaclkes() {
        return taclkes;
    }

    public void setTaclkes(int taclkes) {
        this.taclkes = taclkes;
    }

    public int getClearances() {
        return clearances;
    }

    public void setClearances(int clearances) {
        this.clearances = clearances;
    }

    public int getYellowCards() {
        return yellowCards;
    }

    public void setYellowCards(int yellowCards) {
        this.yellowCards = yellowCards;
    }

    public int getRedCards() {
        return redCards;
    }

    public void setRedCards(int redCards) {
        this.redCards = redCards;
    }

    public int getFoulsCommited() {
        return foulsCommited;
    }

    public void setFoulsCommited(int foulsCommited) {
        this.foulsCommited = foulsCommited;
    }

    public String getTactics() {
        return tactics;
    }

    public void setTactics(String tactics) {
        this.tactics = tactics;
    }

    public List<sdaacademy.model.FootballPlayer> getStartingEleven() {
        return startingEleven;
    }

    public void setStartingEleven(List<sdaacademy.model.FootballPlayer> startingEleven) {
        this.startingEleven = startingEleven;
    }

    public List<sdaacademy.model.FootballPlayer> getSubstitutes() {
        return substitutes;
    }

    public void setSubstitutes(List<sdaacademy.model.FootballPlayer> substitutes) {
        this.substitutes = substitutes;
    }

    @Override
    public String toString() {
        return "TeamStatistics{" +
                "country='" + country + '\'' +
                ", attemptsOnGoal=" + attemptsOnGoal +
                ", onTarget=" + onTarget +
                ", offTarget=" + offTarget +
                ", blocked=" + blocked +
                ", corners=" + corners +
                ", offsides=" + offsides +
                ", ballPassion=" + ballPassion +
                ", passAccuracy=" + passAccuracy +
                ", numPasses=" + numPasses +
                ", passesCompleted=" + passesCompleted +
                ", distanceCovered=" + distanceCovered +
                ", taclkes=" + taclkes +
                ", clearances=" + clearances +
                ", yellowCards=" + yellowCards +
                ", redCards=" + redCards +
                ", foulsCommited=" + foulsCommited +
                ", tactics='" + tactics + '\'' +
                ", startingEleven=" + startingEleven +
                ", substitutes=" + substitutes +
                '}';
    }
}

/*
"home_team_statistics": {
"country": "France",
"attempts_on_goal": 21,
"on_target": 8,
"off_target": 8,
"blocked": 5,
"corners": 13,
"offsides": 2,
"ball_possession": 60,
"pass_accuracy": 87,
"num_passes": 619,
"passes_completed": 540,
"distance_covered": 103,
"tackles": 10,
"clearances": 7,
"yellow_cards": 0,
"red_cards": 0,
"fouls_committed": 11,
"tactics": "4-3-3",
"starting_eleven": [],
"substitutes": []
 */
