package sdaacademy.usecase;

import sdaacademy.model.Match;
import sdaacademy.model.Team;

import java.util.List;
import java.util.stream.Stream;

public class MatchUseCase {
    public static void findTeamsWithRedCard(List<Match> matches) {
        matches.stream()
                .flatMap((match) -> {
                    //return Arrays.asList(match.getHomeTeamStatistics(),
                    // match.getAwayTeamStatistics()).stream();
                    return Stream.of(match.getHomeTeamStatistics(),
                            match.getAwayTeamStatistics());
                })
                .filter(teamStatistics -> teamStatistics.getRedCards() > 0)
                .forEach(System.out::println);
    }

    public static void findTeamsWithRedOrYellowCards(List<Match> matches){
        System.out.println("Znajdź wszystkie zespołu których zawodnicy podczas meczu nie otrzymali ani jednej żółtej i czerwonej kartki");
        matches.stream()
                .flatMap((match) -> {
                    //return Arrays.asList(match.getHomeTeamStatistics(),
                    // match.getAwayTeamStatistics()).stream();
                    return Stream.of(match.getHomeTeamStatistics(),
                            match.getAwayTeamStatistics());
                })
                .filter(teamStatistics -> teamStatistics.getRedCards() > 0)
                .forEach(System.out::println);
    }
}


