package sdaacademy.usecase;

import sdaacademy.model.Team;

import java.util.List;

public class TeamUseCase {

    public static void showTeams(List<Team> teams) {
        System.out.println("Lista zespołów");
        teams.stream()
                .forEach(System.out::println);
    }
}
