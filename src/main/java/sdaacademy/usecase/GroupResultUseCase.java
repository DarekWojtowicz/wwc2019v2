package sdaacademy.usecase;

import sdaacademy.model.Group;
import sdaacademy.model.Team;

import java.util.List;

public class GroupResultUseCase {


    public static void findTeamsWithoutDraw(List<Group> groups) {
        groups.stream().flatMap((Group group) -> {
            return group.getTeams().stream();
        }).filter((Team team) -> {
            return team.getDraws() == 0;
        }).forEach(System.out::println);
    }
}
