package sdaacademy.command;

import sdaacademy.model.Group;
import sdaacademy.network.HttpClient;

public class GetGroupResultsCommand extends BaseCommand<Group> {

    public GetGroupResultsCommand(HttpClient httpClient) {
        super(httpClient);
    }

    @Override
    String getUrl() {
        return "https://worldcup.sfg.io/teams/group_results";
    }

    @Override
    Class<Group[]> getClassToDeserialize() {
        return Group[].class;
    }
}
