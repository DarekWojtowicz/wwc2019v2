package sdaacademy.command;

import sdaacademy.model.Match;
import sdaacademy.network.HttpClient;


public class GetMatchesCommand extends BaseCommand<Match> {

    public GetMatchesCommand(HttpClient httpClient) {
        super(httpClient);
    }

    @Override
    String getUrl() {
        return "https://worldcup.sfg.io/matches";
    }

    @Override
    Class<Match[]> getClassToDeserialize() {
        return Match[].class;
    }
}
