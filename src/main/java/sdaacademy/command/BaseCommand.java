package sdaacademy.command;

import com.google.gson.Gson;
import sdaacademy.model.Group;
import sdaacademy.network.HttpClient;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public abstract class BaseCommand<T> implements Command<T> {

    protected HttpClient httpClient;

    BaseCommand(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    abstract String getUrl();

    abstract Class<T[]> getClassToDeserialize();

    @Override
    public List<T> fetch() throws IOException {
        Gson gson = new Gson();
        String response = httpClient.makeRequest(getUrl());
        T[] results = gson.fromJson(response, getClassToDeserialize());
        return Arrays.asList(results);
    }
}
