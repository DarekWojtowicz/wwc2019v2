package sdaacademy.command;

import sdaacademy.model.Group;
import sdaacademy.model.Team;
import sdaacademy.network.HttpClient;


public class GetTeamsCommand extends BaseCommand<Team> {

    public GetTeamsCommand(HttpClient httpClient) {
        super(httpClient);
    }

    @Override
    String getUrl() {
        return "https://worldcup.sfg.io/teams/";
    }

    @Override
    Class<Team[]> getClassToDeserialize() {
        return Team[].class;
    }
}
